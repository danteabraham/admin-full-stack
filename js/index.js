$(function(){
  $('.carousel').carousel({ interval: 3000 });
  $("[data-toggle='tooltip']").tooltip();
});

$('#contacto').on('show.bs.modal', function(e) {
  $('#contactoBtn').removeClass('btn-outline-success');
  $('#contactoBtn').addClass('btn-primary');
  $('#contactoBtn').prop('disabled', true);
});

$('#contacto').on('show.bs.modal', function(e) {
  console.log('el modal contacto se mostro');
});

$('#contacto').on('hide.bs.modal', function(e) {
  console.log('el modal contacto se oculto');
});

$('#contacto').on('hidden.bs.modal', function(e) {
  //console.log('el modal contacto se mostro');
  $('#contactoBtn').prop('disabled', false);
});